import hashlib
import json
import random
import string
import time
import uuid
import os

from settings import log, CONFIG, req
from notify import Notify


def version():
    return 'v1.6.11'

class Base(object):
    def __init__(self, cookies: str = None):
        if not isinstance(cookies, str):
            raise TypeError('%s want a %s but got %s' %
                            (self.__class__, type(__name__), type(cookies)))
        self._cookie = cookies

    def get_header(self):
        header = {
            'User-Agent': CONFIG.USER_AGENT,
            'Referer': CONFIG.REFERER_URL,
            'Accept-Encoding': 'gzip, deflate, br',
            'Cookie': self._cookie
        }
        return header


class Roles(Base):
    def get_awards(self):
        response = {}
        try:
            response = req.request('get', CONFIG.AWARD_URL, headers=self.get_header()).json()
        except json.JSONDecodeError as e:
            raise Exception(e)

        return response

    def get_roles(self):
        log.info('Ready to get account information...')
        response = {}
        try:
            response = req.request('get', CONFIG.ROLE_URL, headers=self.get_header()).json()
            message = response['message']
        except Exception as e:
            raise Exception(e)
        if response.get(
            'retcode', 1) != 0 or response.get('data', None) is None:
            raise Exception(message)
        log.info('Acquiring account information is complete')
        return response


class Sign(Base):
    def __init__(self, cookies: str = None):
        super(Sign, self).__init__(cookies)
        self._region_list = []
        self._region_name_list = []
        self._uid_list = []

    @staticmethod
    def get_ds():
        # v2.3.0-web @povsister & @journey-ad
        n = 'h8w582wxwgqvahcdkpvdhbh2w9casgfl'
        i = str(int(time.time()))
        r = ''.join(random.sample(string.ascii_lowercase + string.digits, 6))
        c = hexdigest('salt=' + n + '&t=' + i + '&r=' + r)
        return '{},{},{}'.format(i, r, c)

    def get_header(self):
        header = super(Sign, self).get_header()
        header.update({
            'x-rpc-device_id':str(uuid.uuid3(
                uuid.NAMESPACE_URL, self._cookie)).replace('-', '').upper(),
            # 1:  ios
            # 2:  android
            # 4:  pc web
            # 5:  mobile web
            'x-rpc-client_type': '5',
            'x-rpc-app_version': CONFIG.APP_VERSION,
            'DS': self.get_ds(),
        })
        return header

    def get_info(self):
        user_game_roles = Roles(self._cookie).get_roles()
        role_list = user_game_roles.get('data', {}).get('list', [])

        # role list empty
        if not role_list:
            raise Exception(user_game_roles.get('message', 'Role list empty'))

        log.info(f'The current account is bound {len(role_list)} Roles')
        # Change Region List
        info_list = ['cn_gf01','cn_qd01']
        # cn_gf01:  Sky island
        # cn_qd01:  World Tree
        self._region_list = [(i.get('region', 'NA')) for i in role_list]
        self._region_name_list = [(i.get('region_name', 'NA'))
            for i in role_list]
        self._uid_list = [(i.get('game_uid', 'NA')) for i in role_list]

        log.info('Ready to get check-in information...')
        for i in range(len(self._uid_list)):
            info_url = CONFIG.INFO_URL.format(
                self._region_list[i], CONFIG.ACT_ID, self._uid_list[i])
            try:
                content = req.request('get', info_url, headers=self.get_header()).json()
                info_list.append(content)
            except Exception as e:
                raise Exception(e)

        if not info_list:
            raise Exception('User sign info list is empty')
        log.info('The sign-in information is obtained')
        return info_list

    def run(self):
        info_list = self.get_info()
        message_list = []
        for i in range(len(info_list)):
            today = info_list[i]['data']['today']
            total_sign_day = info_list[i]['data']['total_sign_day']
            awards = Roles(self._cookie).get_awards()['data']['awards']
            uid = str(self._uid_list[i]).replace(
                str(self._uid_list[i])[1:8], '******', 1)

            log.info(f'Ready for travelers {i + 1} Sign in...')
            time.sleep(10)
            message = {
                'today': today,
                'region_name': self._region_name_list[i],
                'uid': uid,
                'total_sign_day': total_sign_day,
                'end': '',
            }
            if info_list[i]['data']['is_sign'] is True:
                message['award_name'] = awards[total_sign_day - 1]['name']
                message['award_cnt'] = awards[total_sign_day - 1]['cnt']
                message['status'] = f'👀 Traveler {i + 1} number, You have already signed in'
                message_list.append(self.message.format(**message))
                continue
            else:
                message['award_name'] = awards[total_sign_day]['name']
                message['award_cnt'] = awards[total_sign_day]['cnt']
            if info_list[i]['data']['first_bind'] is True:
                message['status'] = f'💪 Traveler {i + 1} number, Please go to HoYoLAB community to manually sign in once'
                message_list.append(self.message.format(**message))
                continue

            data = {
                'act_id': CONFIG.ACT_ID,
                'region': self._region_list[i],
                'uid': self._uid_list[i]
            }

            try:
                response = req.request('post', CONFIG.SIGN_URL, headers=self.get_header(),
                    data=json.dumps(data, ensure_ascii=False)).json()
            except Exception as e:
                raise Exception(e)
            code = response.get('retcode', 99999)
            # 0:      success
            # -5003:  already signed in
            if code != 0:
                message_list.append(response)
                continue
            message['total_sign_day'] = total_sign_day + 1
            message['status'] = response['message']
            message_list.append(self.message.format(**message))
        log.info('Sign in')

        return ''.join(message_list)

    @property
    def message(self):
        return CONFIG.MESSAGE_TEMPLATE


if __name__ == '__main__':
    log.info(f'🌀HoYoLAB sign-in assistant {version()}')
    log.info('If the sign-in fails, Please try to update!')
    log.info('Mission start')
    notify = Notify()
    msg_list = []
    ret = success_num = fail_num = 0
    """HoYoLAB Community's COOKIE
    :param OS_COOKIE: The COOKIE of HoYoLAB Community. The COOKIE values of multiple accounts are separated by # signs, for example: 1#2#3#4
    """
    # Github Actions users, please go to Repo's Settings->Secrets to set variables, the variable name must be exactly the same as the above parameter variable name, otherwise it will be invalid!!!
    # Name=<variable name>,Value=<obtained value>
    COOKIE = ''

    if os.environ.get('COOKIE', '') != '':
        COOKIE = os.environ['COOKIE']

    cookie_list = COOKIE.split('#')
    log.info(f'A total of configured {len(cookie_list)} Accounts')
    for i in range(len(cookie_list)):
        log.info(f'Ready for NO.{i + 1} Account sign in...')
        try:
            msg = f'NO.{i + 1} account number:{Sign(cookie_list[i]).run()}'
            msg_list.append(msg)
            success_num = success_num + 1
        except Exception as e:
            msg = f'NO.{i + 1} account number:\n    {e}'
            msg_list.append(msg)
            fail_num = fail_num + 1
            log.error(msg)
            ret = -1
        continue
    notify.send(status=f'success: {success_num} | failure: {fail_num}', msg=msg_list)
    if ret != 0:
        log.error('Abnormal exit')
        exit(ret)
    log.info('End of mission')